import accountApi from "api/accontApi";
import React, { useEffect, useState } from "react";
import { Container, Table } from "react-bootstrap";
import "./Account.scss";

function Account() {
  const [username, setUsername] = useState("");
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [birthday, setBirthday] = useState("");

  useEffect(() => {
    const getData = async () => {
      await accountApi
        .get()
        .then((res) => {
          const data = res.data;
          setUsername(data.username);
          setFirstname(data.firstname);
          setLastname(data.lastname);
          setPhoneNumber(data.phoneNumber);
          setBirthday(data.birthday);
        })
        .catch((error) => console.log(error.message));
    };
    getData();
  }, []);

  return (
    <div className="account">
      <Container className>
        <Table striped bordered hover size="sm">
          <thead>
            <tr>
              <th>Label</th>
              <th>Data</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Username</td>
              <td>{username}</td>
            </tr>
            <tr>
              <td>Name</td>
              <td>{lastname + " " + firstname}</td>
            </tr>
            <tr>
              <td>Date of birth</td>
              <td>{birthday}</td>
            </tr>
            <tr>
              <td>Phone number</td>
              <td>{phoneNumber}</td>
            </tr>
          </tbody>
        </Table>
      </Container>
    </div>
  );
}

export default Account;
