import accountApi from "api/accontApi";
import InputField from "components/Form/InputField";
import Notification from "components/Notification";
import { FastField, Form, Formik } from "formik";
import PropTypes from "prop-types";
import React, { useState } from "react";
import { Button, Col, Row } from "react-bootstrap";
import { Redirect } from "react-router-dom";
import * as Yup from "yup";
import "./Register.scss";

Register.propTypes = {
  isLogin: PropTypes.bool,
  onLogin: PropTypes.func,
};

Register.defaultProps = {
  isLogin: false,
  onLogin: null,
};

function Register(props) {
  const { onLogin, isLogin } = props;

  const [isShow, setIsShow] = useState(false);
  const [isError, setIsError] = useState(false);

  const initialValues = {
    username: "",
    password: "",
    repassword: "",
    firstname: "",
    lastname: "",
    birthday: "",
    phoneNumber: "",
  };

  const validationSchema = Yup.object().shape({
    username: Yup.string()
      .min(6, "Username must have at least 6 characters")
      .required("Username is required."),
    password: Yup.string()
      .min(6, "Password must have at least 6 characters")
      .required("Password is required."),
    repassword: Yup.string().oneOf(
      [Yup.ref("password"), null],
      "Passwords is not match"
    ),
    firstname: Yup.string(),
    lastname: Yup.string(),
    birthday: Yup.date(),
    phoneNumber: Yup.number(),
  });

  function handleSubmit(values) {
    accountApi
      .register(values)
      .then((res) => {
        const token = res.data.token;
        onLogin(token);
      })
      .catch((error) => {
        setIsError(true);
        setIsShow(true);
        setTimeout(function () {
          setIsError(false);
          setIsShow(false);
        }, 2500);
      });
  }

  if (isLogin) return <Redirect to="/" />;

  return (
    <div className="login-page">
      <Notification
        isShow={isShow}
        header={isError ? "Register fail" : "Success"}
        body={
          isError
            ? "Username already used by someone else."
            : "Sign in successfully."
        }
      />

      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {(formikProps) => {
          return (
            <Form className="login-card">
              <h1>Register</h1>
              <FastField
                name="username"
                component={InputField}
                label="Username"
                placeholder="Enter your username"
              />

              <FastField
                name="password"
                type="password"
                component={InputField}
                label="Password"
                placeholder="Enter your password"
              />

              <FastField
                name="repassword"
                type="password"
                component={InputField}
                label="Confirm password"
                placeholder="Enter confirm password"
              />

              <Row>
                <Col>
                  <FastField
                    name="firstname"
                    component={InputField}
                    label="First name"
                    placeholder="Enter your first name"
                  />
                </Col>
                <Col>
                  <FastField
                    name="lastname"
                    component={InputField}
                    label="Last name"
                    placeholder="Enter your last name"
                  />
                </Col>
              </Row>

              <FastField
                name="birthday"
                type="date"
                component={InputField}
                label="Date of birth"
                placeholder="Enter your birthday"
              />
              <FastField
                name="phoneNumber"
                component={InputField}
                label="Phone number"
                placeholder="Enter your phone number"
              />

              <Button type="submit">Submit</Button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
}

export default Register;
