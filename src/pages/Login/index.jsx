import accountApi from "api/accontApi";
import InputField from "components/Form/InputField";
import Notification from "components/Notification";
import { FastField, Form, Formik } from "formik";
import PropTypes from "prop-types";
import React, { useState } from "react";
import { Button, Col, Row } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import * as Yup from "yup";
import "./Login.scss";

Login.propTypes = {
  isLogin: PropTypes.bool,
  onLogin: PropTypes.func,
};

Login.defaultProps = {
  isLogin: false,
  onLogin: null,
};

function Login(props) {
  const { onLogin, isLogin } = props;
  const [isShow, setIsShow] = useState(false);
  const [isError, setIsError] = useState(false);

  const initialValues = {
    username: "",
    password: "",
  };

  const validationSchema = Yup.object().shape({
    username: Yup.string()
      .min(6, "Username must have at least 6 characters")
      .required("Username is required."),
    password: Yup.string()
      .min(6, "Password must have at least 6 characters")
      .required("Password is required."),
  });

  function handleSubmit(values) {
    accountApi
      .login(values)
      .then((res) => {
        const token = res.data.token;
        onLogin(token);
      })
      .catch((error) => {
        setIsError(true);
        setIsShow(true);
        setTimeout(function () {
          setIsError(false);
          setIsShow(false);
        }, 2500);
      });
  }

  if (isLogin) return <Redirect to="/" />;

  return (
    <div className="login-page">
      <Notification
        isShow={isShow}
        header={isError ? "Login fail" : "Success"}
        body={
          isError
            ? "Username or password incorrect."
            : "Logged in successfully."
        }
      />

      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {(formikProps) => {
          return (
            <Form className="login-card">
              <h1>Login</h1>
              <FastField
                name="username"
                component={InputField}
                label="Username"
                placeholder="Enter your username"
              />

              <FastField
                name="password"
                type="password"
                component={InputField}
                label="Password"
                placeholder="Enter your password"
              />

              <Row>
                <Col>
                  <Link to="/register">
                    <Button variant="warning" type="submit">
                      Signup
                    </Button>
                  </Link>
                </Col>
                <Col>
                  <Button type="submit">Submit</Button>
                </Col>
              </Row>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
}

export default Login;
