import React from "react";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./Home.scss";

function Home() {
  return (
    <div className="text">
      GAME CENTER{" "}
      <Link to="/games">
        <Button className="play-btn" variant="outline-warning">
          Play Now
        </Button>
      </Link>
    </div>
  );
}

export default Home;
