import React from "react";
import PropTypes from "prop-types";

Clock.propTypes = {
  h: PropTypes.number,
  m: PropTypes.number,
  s: PropTypes.number,
};

Clock.defaultProps = {
  h: 0,
  m: 0,
  s: 0,
};

function Clock(props) {
  const { h, m, s } = props;
  const hh = `0${h}`.slice(-2);
  const mm = `0${m}`.slice(-2);
  const ss = `0${s}`.slice(-2);

  return (
    <span>
      {hh} : {mm} : {ss}
    </span>
  );
}

export default Clock;
