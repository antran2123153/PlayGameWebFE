import React from "react";
import { FcHome } from "react-icons/fc";
import { Link } from "react-router-dom";

function HomeIcon(props) {
  return (
    <Link to="/">
      <FcHome className="user-home" />
    </Link>
  );
}

export default HomeIcon;
