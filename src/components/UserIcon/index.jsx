import PropTypes from "prop-types";
import React, { useState } from "react";
import { ListGroup } from "react-bootstrap";
import { FcPortraitMode } from "react-icons/fc";
import { Link, useHistory } from "react-router-dom";
import "./UserIcon.scss";

UserIcon.propTypes = {
  isLogin: PropTypes.bool,
  onLogout: PropTypes.func,
};

UserIcon.defaultProps = {
  isLogin: false,
  onLogout: null,
};

function UserIcon(props) {
  const history = useHistory();
  const [clickUser, setClickUser] = useState(false);

  const { isLogin, onLogout } = props;

  function handleLogout() {
    setClickUser(!clickUser);
    history.push("/");
    onLogout();
  }

  return (
    <>
      <FcPortraitMode
        className="user-namager"
        onClick={() => {
          setClickUser(!clickUser);
        }}
      />

      {clickUser && (
        <ListGroup className="user-manager-menu">
          <Link
            to="/"
            onClick={() => {
              setClickUser(!clickUser);
            }}
          >
            <ListGroup.Item action>Home Page</ListGroup.Item>
          </Link>
          <Link
            to="/games"
            onClick={() => {
              setClickUser(!clickUser);
            }}
          >
            <ListGroup.Item action>Game Home</ListGroup.Item>
          </Link>
          <Link
            to="/setting"
            onClick={() => {
              setClickUser(!clickUser);
            }}
          >
            <ListGroup.Item action>Setting</ListGroup.Item>
          </Link>
          {isLogin ? (
            <>
              <Link
                to="/account"
                onClick={() => {
                  setClickUser(!clickUser);
                }}
              >
                <ListGroup.Item action>Account</ListGroup.Item>
              </Link>
              <ListGroup.Item action onClick={handleLogout}>
                Logout
              </ListGroup.Item>
            </>
          ) : (
            <>
              <Link
                to="/login"
                onClick={() => {
                  setClickUser(!clickUser);
                }}
              >
                <ListGroup.Item action>Login</ListGroup.Item>
              </Link>
              <Link
                to="/register"
                onClick={() => {
                  setClickUser(!clickUser);
                }}
              >
                <ListGroup.Item action>Sign Up</ListGroup.Item>
              </Link>
            </>
          )}
        </ListGroup>
      )}
    </>
  );
}

export default UserIcon;
