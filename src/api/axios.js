import axios from "axios";

const instance = axios.create({
  baseURL: "https://gamevv-server.herokuapp.com",
});

export default instance;
