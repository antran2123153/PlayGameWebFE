import axios from "./axios";

const vocaApi = {
  add: (body) => {
    console.log(body);
    const url = "/api/voca/add";
    return axios.post(url, {
      en: body.en,
      vn: body.vn,
      type: body.type,
    });
  },

  get: () => {
    const url = "/api/voca";
    return axios.get(url);
  },

  delete: (_id) => {
    const url = "/api/voca/delete";
    return axios.post(url, { _id: _id });
  },

  updateMem: (_id, mem) => {
    const url = "/api/voca/update/mem";
    return axios.post(url, { _id: _id, mem: mem });
  },

  updateShow: (_id, show) => {
    const url = "/api/voca/update/show";
    return axios.post(url, { _id: _id, show: show });
  },
};

export default vocaApi;
