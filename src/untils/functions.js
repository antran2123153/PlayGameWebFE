function randomIndex(size) {
  const i = Math.floor(Math.random() * size);
  const j = Math.floor(Math.random() * size);
  return [i, j];
}

function randomNumber() {
  return Math.random() > 0.2345678 ? 2 : 4;
}

function makeStringAuto(size, str) {
  let s = "";
  for (let i = 0; i < size; i++) s += str + " ";
  return s;
}

function initialArray(size) {
  let array = [];
  for (var i = 0; i < size; ++i) {
    let inArray = [];
    for (var j = 0; j < size; ++j) {
      inArray.push(0);
    }
    array.push(inArray);
  }
  const [x, y] = randomIndex(size);
  array[x][y] = randomNumber();
  return array;
}

function compareArray(arr1, arr2) {
  if (arr1.length !== arr2.length) return false;
  const n = arr1.length;
  for (let i = 0; i < n; i++)
    for (let j = 0; j < n; j++) if (arr1[i][j] !== arr2[i][j]) return false;
  return true;
}

function copyArray(array) {
  const n = array.length;
  let re = [];
  for (let i = 0; i < n; i++) {
    let [...arr1] = array[i];
    re.push(arr1);
  }
  return re;
}

module.exports = {
  makeStringAuto,
  randomNumber,
  randomIndex,
  initialArray,
  compareArray,
  copyArray,
};
