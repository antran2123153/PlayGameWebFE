import HomeIcon from "components/HomeIcon";
import Notification from "components/Notification";
import UserIcon from "components/UserIcon";
import Games from "Games";
import Account from "pages/Account";
import React, { Suspense, useEffect, useState } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";

function App() {
  const [isLogin, setIsLogin] = useState(false);

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (token) {
      setIsLogin(true);
    } else {
      setIsLogin(false);
    }
  }, []);

  function handleLogin(token) {
    localStorage.setItem("token", token);
    setIsLogin(true);
  }

  function handleLogout() {
    localStorage.removeItem("token");
    setIsLogin(false);
  }

  return (
    <Suspense fallback={<div>Loading ...</div>}>
      <BrowserRouter>
        <HomeIcon />
        <UserIcon isLogin={isLogin} onLogout={handleLogout} />

        <Notification />

        <Switch>
          <Route path="/games" component={Games} />
          <Route
            path="/login"
            render={() => <Login isLogin={isLogin} onLogin={handleLogin} />}
          />
          <Route
            path="/register"
            render={() => <Register isLogin={isLogin} onLogin={handleLogin} />}
          />
          <Route path="/account" component={Account} />
          <Route path="/" component={Home} />
        </Switch>
      </BrowserRouter>
    </Suspense>
  );
}

export default App;
