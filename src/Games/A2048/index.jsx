import React, { useEffect, useState } from "react";
import KeyboardEventHandler from "react-keyboard-event-handler";
import { useSwipeable } from "react-swipeable";
import "./A2048.scss";
import Array from "./components/Array";
import GameOver from "./components/GameOver";
import GroupButton from "./components/GroupButon";
import Menu from "./components/Menu";

function string2array(str, size) {
  str = str.split(",");
  let array = [];
  for (var i = 0; i < size; ++i) {
    let inArray = [];
    for (var j = 0; j < size; ++j) inArray.push(parseInt(str[i * size + j]));
    array.push(inArray);
  }
  console.log(array);
  return array;
}

function randomIndex(size) {
  const i = Math.floor(Math.random() * size);
  const j = Math.floor(Math.random() * size);
  return [i, j];
}

function randomNumber(size) {
  if (size === 4) {
    return Math.random() > 0.112345 ? 2 : 4;
  } else if (size === 5) {
    return Math.random() > 0.23456 ? 2 : Math.random() > 0.23456 ? 4 : 8;
  } else {
    return Math.random() > 0.23456
      ? 2
      : Math.random() > 0.23456
      ? 4
      : Math.random() > 0.23456
      ? 8
      : 16;
  }
}

function makeStringAuto(size, str) {
  let s = "";
  for (let i = 0; i < size; i++) s += str + " ";
  return s;
}

function initialArray(size) {
  let array = [];
  for (var i = 0; i < size; ++i) {
    let inArray = [];
    for (var j = 0; j < size; ++j) {
      inArray.push(0);
    }
    array.push(inArray);
  }
  const [x, y] = randomIndex(size);
  array[x][y] = randomNumber(size);
  return array;
}

const COLORS = [
  "#ffffff",
  "#ffffe6",
  "#ffff66",
  "#ffe066",
  "#ffcc00",
  "#ffa64d",
  "#ff8000",
  "#ff751a",
  "#e65c00",
  "#ff5050",
  "#ff0000",
  "#e60073",
  "#800040",
  "#99004d",
  "#660033",
  "#33001a",
  "#000000",
];

function A2048() {
  const [array, setArray] = useState(initialArray(4));
  const [score, setScore] = useState(0);
  const [isLose, setIsLose] = useState(false);
  const [size, setSize] = useState(4);
  const [second, setSecond] = useState(0);
  const [minute, setMinute] = useState(0);
  const [hour, setHour] = useState(0);

  useEffect(() => {
    const saveArray = localStorage.getItem("2048array");
    const saveSize = localStorage.getItem("2048size");
    const saveScore = localStorage.getItem("2048scoreSave");
    if (saveArray && saveSize && saveScore) {
      const arr = string2array(saveArray, parseInt(saveSize));
      setArray(arr);
      setScore(parseInt(saveScore));
      setSize(parseInt(saveSize));
    }
  }, []);

  useEffect(() => {
    const clock = setInterval(() => {
      if (second === 59) {
        setSecond(0);
        if (minute === 59) {
          setMinute(0);
          setHour(hour + 1);
        } else setMinute(minute + 1);
      } else setSecond(second + 1);
    }, 1000);

    return () => clearInterval(clock);
  }, [second, minute, hour]);

  function handleGameOver() {
    for (let i = 0; i < size; i++)
      for (let j = 0; j < size - 1; j++) {
        if (array[i][j] === array[i][j + 1]) return;
      }

    for (let i = 0; i < size; i++)
      for (let j = 0; j < size - 1; j++) {
        if (array[j][i] === array[j + 1][i]) return;
      }
    setIsLose(true);
    const maxScore = localStorage.getItem("2048maxScore");
    if (maxScore) {
      console.log(score, maxScore);
      if (score > parseInt(maxScore))
        localStorage.setItem("2048maxScore", score);
    } else {
      localStorage.setItem("2048maxScore", score);
      localStorage.setItem(
        "2048time",
        ` ${size}x${size} ${hour}:${minute}:${second} `
      );
    }
  }

  function createNewItem() {
    let arr = [...array];
    let count = 0;
    for (let i of arr) for (let j of i) if (j !== 0) count++;
    if (count === size * size) handleGameOver();
    else {
      while (true) {
        const [i, j] = randomIndex(size);
        if (arr[i][j] === 0) {
          arr[i][j] = randomNumber(size);
          break;
        }
      }
      setArray(arr);
    }
  }

  function handleLeft() {
    let arr = [...array];
    for (let i = 0; i < size; i++) {
      for (let j = 0; j < size; j++) {
        let f = j;
        while (f < size && arr[i][f] === 0) f++;
        if (f === size) break;
        let k = f + 1;
        while (k < size && arr[i][k] === 0) k++;
        if (k === size) {
          arr[i][j] = arr[i][f];
          if (f !== j) arr[i][f] = 0;
        } else if (arr[i][f] === arr[i][k]) {
          arr[i][j] = arr[i][f] * 2;
          if (f !== j) arr[i][f] = 0;
          arr[i][k] = 0;
          setScore(score + arr[i][j]);
        } else {
          arr[i][j] = arr[i][f];
          arr[i][j + 1] = arr[i][k];
          if (f !== j && f !== j + 1) arr[i][f] = 0;
          if (k !== j && k !== j + 1) arr[i][k] = 0;
        }
      }
    }
  }

  function handleRight() {
    let arr = [...array];
    for (let i = 0; i < size; i++) {
      for (let j = size - 1; j >= 0; j--) {
        let f = j;
        while (f >= 0 && arr[i][f] === 0) f--;
        if (f === -1) break;
        let k = f - 1;
        while (k >= 0 && arr[i][k] === 0) k--;
        if (k === -1) {
          arr[i][j] = arr[i][f];
          if (f !== j) arr[i][f] = 0;
        } else if (arr[i][f] === arr[i][k]) {
          arr[i][j] = arr[i][f] * 2;
          if (f !== j) arr[i][f] = 0;
          arr[i][k] = 0;
          setScore(score + arr[i][j]);
        } else {
          arr[i][j] = arr[i][f];
          arr[i][j - 1] = arr[i][k];
          if (f !== j && f !== j - 1) arr[i][f] = 0;
          if (k !== j && k !== j - 1) arr[i][k] = 0;
        }
      }
    }
  }

  function handleUp() {
    let arr = [...array];
    for (let i = 0; i < size; i++) {
      for (let j = 0; j < size - 1; j++) {
        let f = j;
        while (f < size && arr[f][i] === 0) f++;
        if (f === size) break;
        let k = f + 1;
        while (k < size && arr[k][i] === 0) k++;
        if (k === size) {
          arr[j][i] = arr[f][i];
          if (f !== j) arr[f][i] = 0;
        } else if (arr[f][i] === arr[k][i]) {
          arr[j][i] = arr[f][i] * 2;
          if (f !== j) arr[f][i] = 0;
          arr[k][i] = 0;
          setScore(score + arr[i][j]);
        } else {
          arr[j][i] = arr[f][i];
          arr[j + 1][i] = arr[k][i];
          if (f !== j && f !== j + 1) arr[f][i] = 0;
          if (k !== j && k !== j + 1) arr[k][i] = 0;
        }
      }
    }
  }

  function handleDown() {
    let arr = [...array];
    for (let i = 0; i < size; i++) {
      for (let j = size - 1; j >= 0; j--) {
        let f = j;
        while (f >= 0 && arr[f][i] === 0) f--;
        if (f === -1) break;
        let k = f - 1;
        while (k >= 0 && arr[k][i] === 0) k--;
        if (k === -1) {
          arr[j][i] = arr[f][i];
          if (f !== j) arr[f][i] = 0;
        } else if (arr[f][i] === arr[k][i]) {
          arr[j][i] = arr[f][i] * 2;
          if (f !== j) arr[f][i] = 0;
          arr[k][i] = 0;
          setScore(score + arr[i][j]);
        } else {
          arr[j][i] = arr[f][i];
          arr[j - 1][i] = arr[k][i];
          if (f !== j && f !== j - 1) arr[f][i] = 0;
          if (k !== j && k !== j - 1) arr[k][i] = 0;
        }
      }
    }
  }

  function handleKeydown(key, e) {
    if (key === "up") {
      handleUp();
    } else if (key === "down") {
      handleDown();
    } else if (key === "left") {
      handleLeft();
    } else if (key === "right") {
      handleRight();
    }
    createNewItem();
    localStorage.setItem("2048array", array.toString());
    localStorage.setItem("2048size", size.toString());
    localStorage.setItem("2048scoreSave", score.toString());
  }

  function handleNewGame() {
    setArray(initialArray(size));
    setScore(0);
    setIsLose(false);
    setMinute(0);
    setSecond(0);
    setHour(0);
  }

  function handleUpSize() {
    const newSize = size === 6 ? 6 : size + 1;
    setSize(newSize);
    setArray(initialArray(newSize));
    setScore(0);
    setIsLose(false);
    setMinute(0);
    setSecond(0);
    setHour(0);
  }

  function handleDownSize() {
    const newSize = size === 4 ? 4 : size - 1;
    setSize(newSize);
    setArray(initialArray(newSize));
    setScore(0);
    setIsLose(false);
    setMinute(0);
    setSecond(0);
    setHour(0);
  }

  const handleSwipe = useSwipeable({
    onSwipedLeft: () => handleKeydown("left"),
    onSwipedRight: () => handleKeydown("right"),
    onSwipedUp: () => handleKeydown("up"),
    onSwipedDown: () => handleKeydown("down"),
    preventDefaultTouchmoveEvent: true,
    trackMouse: true,
  });

  return (
    <div {...handleSwipe} className="a2048">
      <h1 className="header">2048</h1>

      <Menu
        second={second}
        hour={hour}
        minute={minute}
        score={score}
        size={size}
      />

      {isLose ? (
        <GameOver score={score} onNewGame={handleNewGame} />
      ) : (
        <div
          className="grid-container"
          style={{
            gridTemplateColumns: makeStringAuto(size, "auto"),
          }}
        >
          <Array array={array} colors={COLORS} />
        </div>
      )}

      <GroupButton
        onNewGame={handleNewGame}
        onUpSize={handleUpSize}
        onDownSize={handleDownSize}
      />

      <KeyboardEventHandler
        handleKeys={["up", "down", "left", "right"]}
        onKeyEvent={handleKeydown}
      />
    </div>
  );
}

export default A2048;
