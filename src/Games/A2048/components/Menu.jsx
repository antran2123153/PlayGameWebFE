import Clock from "components/Clock";
import PropTypes from "prop-types";
import React from "react";

Menu.propTypes = {
  score: PropTypes.number,
  size: PropTypes.number,
  second: PropTypes.number,
  minute: PropTypes.number,
  hour: PropTypes.number,
};

Menu.defaultProps = {
  score: 0,
  size: 4,
  second: 0,
  minute: 0,
  hour: 0,
};

function Menu(props) {
  const { score, second, minute, hour } = props;

  return (
    <div className="menu-2048">
      <b>
        Score : <span className="menu-span-2048">{score}</span>
      </b>
      <b>
        Top :{" "}
        <span className="menu-span-2048">
          {localStorage.getItem("2048maxScore") +
            localStorage.getItem("2048time")}
        </span>
      </b>
      <b>
        Time :{" "}
        <span className="menu-span-2048">
          <Clock h={hour} m={minute} s={second} />
        </span>
      </b>
    </div>
  );
}

export default Menu;
