import PropTypes from "prop-types";
import React from "react";

Array.propTypes = {
  array: PropTypes.array,
};

Array.defaultProps = {
  array: [
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
  ],
};

function Array(props) {
  const { array, colors } = props;

  const view = array.map((row) => {
    const cols = row.map((col, index) => {
      if (col === 0)
        return (
          <div
            className="grid-item-hidden"
            key={index}
            style={{ backgroundColor: colors[0] }}
          ></div>
        );
      else
        return (
          <div
            className="grid-item"
            key={index}
            style={{ backgroundColor: colors[Math.log(col) / Math.log(2)] }}
          >
            {col}
          </div>
        );
    });
    return cols;
  });
  return view;
}

export default Array;
