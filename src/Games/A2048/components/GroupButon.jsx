import PropTypes from "prop-types";
import React, { useState } from "react";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";

GroupButton.propTypes = {
  onNewGame: PropTypes.func,
  onUpSize: PropTypes.func,
  onDownSize: PropTypes.func,
};

GroupButton.defaultProps = {
  onNewGame: null,
  onUpSize: null,
  onDownSize: null,
};

function GroupButton(props) {
  const { onNewGame, onDownSize, onUpSize } = props;

  const [show, setShow] = useState(false);

  function handleClick() {
    setShow(!show);
  }

  return (
    <div className="group-btn">
      <Button onClick={handleClick}>Menu</Button>
      {show && (
        <>
          <Button
            onClick={onNewGame}
            variant="outline-warning"
            className="menu-btn"
          >
            New Game
          </Button>
          <Button variant="outline-success" onClick={onDownSize}>
            Down size
          </Button>
          <Button variant="outline-primary" onClick={onUpSize}>
            Up size
          </Button>
          <Link to="/" className="menu-btn">
            <Button variant="outline-danger">Exit</Button>
          </Link>
        </>
      )}
    </div>
  );
}

export default GroupButton;
