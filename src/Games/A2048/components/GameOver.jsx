import { Button } from "react-bootstrap";
import PropTypes from "prop-types";
import React from "react";

GameOver.propTypes = {
  score: PropTypes.number,
  onNewGame: PropTypes.func,
};

GameOver.defaultProps = {
  score: 0,
  onNewGame: null,
};

function GameOver(props) {
  const { score, onNewGame } = props;
  return (
    <div className="game-over">
      GAMEOVER!!! <br />
      Score: {score} <br />
      <Button onClick={onNewGame} variant="warning">
        New Game
      </Button>
    </div>
  );
}

export default GameOver;
