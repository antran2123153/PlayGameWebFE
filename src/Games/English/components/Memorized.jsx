import PropTypes from "prop-types";
import React, { useState } from "react";
import { Button, ListGroup } from "react-bootstrap";
import { FcApprove, FcNightPortrait } from "react-icons/fc";

Memorized.propTypes = {
  vocas: PropTypes.array,
  onDelete: PropTypes.func,
  onMenorized: PropTypes.func,
};

Memorized.defaultProps = {
  vocas: [],
  onDelete: null,
  onMenorized: null,
};

function Memorized(props) {
  const { vocas, onDelete, onMenorized } = props;
  const [show, setShow] = useState(false);

  function handleShow() {
    setShow(!show);
  }

  const view = vocas.map((voca) => {
    return (
      <ListGroup.Item key={voca._id}>
        <span className="memorized-return-del-btn">
          <FcNightPortrait title="Back" onClick={() => onMenorized(voca._id)} />{" "}
          <FcApprove title="OK" onClick={() => onDelete(voca._id)} />
        </span>
        {voca.en} ({voca.type}) : {voca.vn}
      </ListGroup.Item>
    );
  });

  return (
    <>
      <Button className="memorized-btn" onClick={handleShow} variant="warning">
        Memorized
      </Button>
      {show && <ListGroup className="menorized">{view}</ListGroup>}
    </>
  );
}

export default Memorized;
