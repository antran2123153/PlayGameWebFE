import PropTypes from "prop-types";
import React from "react";
import { Card, Container } from "react-bootstrap";
import { FcApproval } from "react-icons/fc";

Vocas.propTypes = {
  vocas: PropTypes.array,
  handleClick: PropTypes.func,
  onMenorized: PropTypes.func,
};

Vocas.defaultProps = {
  vocas: [],
  handleClick: null,
  onMenorized: null,
};

function Vocas(props) {
  const { vocas, handleClick, onMenorized } = props;

  const view = vocas.map((voca) => {
    return (
      <Card
        className={voca.show ? "card-word-vn" : "card-word-en"}
        key={voca._id}
        onClick={() => handleClick(voca._id)}
      >
        <FcApproval
          className="voca-oke-icon"
          title="Memorized"
          onClick={() => onMenorized(voca._id)}
        />
        {voca.show ? `(${voca.type}) ${voca.vn}` : voca.en}
      </Card>
    );
  });

  return <Container className="english-body">{view}</Container>;
}

export default Vocas;
