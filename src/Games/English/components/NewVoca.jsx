import InputField from "components/Form/InputField";
import { FastField, Form, Formik } from "formik";
import PropTypes from "prop-types";
import React, { useState } from "react";
import { Button } from "react-bootstrap";
import * as Yup from "yup";
import vocaApi from "api/vocaApi";

NewVoca.propTypes = {
  getData: PropTypes.func,
};

NewVoca.defaultProps = {
  getData: null,
};

function NewVoca(props) {
  const [show, setShow] = useState(false);

  const { getData } = props;

  const initialValues = {
    en: "",
    vn: "",
    type: "",
  };

  const validationSchema = Yup.object().shape({
    en: Yup.string().required("English is required."),
    vn: Yup.string().required("Vietnam is required."),
  });

  function handleSubmit(values) {
    vocaApi
      .add(values)
      .then((res) => {
        getData();
        handleShow();
      })
      .catch((error) => console.log(error.message));
  }

  function handleShow() {
    setShow(!show);
  }

  return (
    <>
      <Button className="new-voca-btn" onClick={handleShow} variant="info">
        New Voca
      </Button>
      {show && (
        <div className="new-voca">
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
          >
            {(formikProps) => {
              return (
                <Form className="">
                  <Button type="submit">Add</Button>
                  <FastField
                    name="en"
                    component={InputField}
                    placeholder="English"
                  />

                  <FastField
                    name="type"
                    component={InputField}
                    placeholder="type"
                  />

                  <FastField
                    name="vn"
                    component={InputField}
                    placeholder="Vietnam"
                  />
                </Form>
              );
            }}
          </Formik>
        </div>
      )}
    </>
  );
}

export default NewVoca;
