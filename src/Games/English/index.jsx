import vocaApi from "api/vocaApi";
import React, { useEffect, useState } from "react";
import Memorized from "./components/Memorized";
import NewVoca from "./components/NewVoca";
import Vocas from "./components/Vocas";
import "./English.scss";

function English() {
  const [vocas, setVocas] = useState([]);

  async function getData() {
    await vocaApi
      .get()
      .then((res) => {
        setVocas(res.data);
      })
      .catch((error) => console.log(error.message));
  }

  useEffect(() => {
    getData();
  }, []);

  async function handleClick(_id) {
    let newVocas = [...vocas];
    let show = false;
    for (let i = 0; i < newVocas.length; i++)
      if (newVocas[i]._id === _id) {
        newVocas[i].show = !newVocas[i].show;
        show = newVocas[i].show;
        break;
      }
    setVocas(newVocas);

    await vocaApi
      .updateShow(_id, show)
      .then((res) => {
        getData();
      })
      .catch((error) => console.log(error.message));
  }

  async function handleDelete(_id) {
    await vocaApi
      .delete(_id)
      .then((res) => {
        getData();
      })
      .catch((error) => console.log(error.message));
  }

  async function handleMemorized(_id) {
    let newVocas = [...vocas];
    let mem = false;
    for (let i = 0; i < newVocas.length; i++)
      if (newVocas[i]._id === _id) {
        newVocas[i].mem = !newVocas[i].mem;
        mem = newVocas[i].mem;
        break;
      }
    setVocas(newVocas);

    await vocaApi
      .updateMem(_id, mem)
      .then((res) => {
        getData();
      })
      .catch((error) => console.log(error.message));
  }

  const vocasMem = vocas.filter((voca) => voca.mem === true);
  const vocasNoMem = vocas.filter((voca) => voca.mem === false);

  return (
    <div className="english">
      <Vocas
        vocas={vocasNoMem}
        handleClick={handleClick}
        handleMemorized={handleMemorized}
        onMenorized={handleMemorized}
      />

      <Memorized
        vocas={vocasMem}
        onDelete={handleDelete}
        onMenorized={handleMemorized}
      />

      <NewVoca getData={getData} />
    </div>
  );
}

export default English;
