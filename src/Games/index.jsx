import React from "react";
import { Route, Switch, useRouteMatch } from "react-router-dom";
import A2048 from "./A2048";
import Anime from "./Anime";
import English from "./English";
import GameHome from "./GameHome";

function Games() {
  const match = useRouteMatch();
  return (
    <Switch>
      <Route exact path={`${match.url}/2048`} component={A2048} />
      <Route exact path={`${match.url}/anime`} component={Anime} />
      <Route exact path={`${match.url}/english`} component={English} />
      <Route exact path="/games" component={GameHome} />
      <Route component={GameHome} />
    </Switch>
  );
}

export default Games;
