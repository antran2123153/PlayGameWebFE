import React from "react";
import { Card, Col, Row } from "react-bootstrap";
import { GiConsoleController } from "react-icons/gi";
import { useHistory } from "react-router-dom";
import "./GameHome.scss";

function GameHome(props) {
  const history = useHistory();

  const games = [
    {
      url: "/games/2048",
      name: "2048",
      img: "https://raw.githubusercontent.com/antran2123153/image/main/gamevv/2048.jpg",
      description: "Have a little fun with the classic 2048 game.",
    },
    {
      url: "/games/english",
      name: "Toeic's dictionnary",
      img: "https://raw.githubusercontent.com/antran2123153/image/main/gamevv/voca.jpg",
      description:
        "I'm too lazy to learn vocabulary, so I made a game to make learning English vocabulary easier.",
    },
  ];

  function handleClick(url) {
    history.push(url);
  }

  const view = games.map((game, index) => {
    return (
      <Col key={index}>
        <Card className="card-game" onClick={() => handleClick(game.url)}>
          <Card.Img variant="top" src={game.img} />
          <Card.Body>
            <Card.Title>
              <GiConsoleController /> {game.name} <GiConsoleController />
            </Card.Title>
            <Card.Text> {game.description} </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    );
  });

  return (
    <div className="game-home">
      <Row>{view}</Row>
    </div>
  );
}

export default GameHome;
